package co.simplon.promo27.firstspring.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;

//Imports nécessaire pour les tests d'API
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.jupiter.api.Test;



@SpringBootTest //Permet de tester des composant qui utilisent des autowired, va créer une application Spring boot de test
@AutoConfigureMockMvc //Permet de pouvoir récupérer le MockMvc
@Sql("/database.sql") //Remet la base de données à zéro entre chaque test
public class DogControllerTest {

    /**
     * Le MockMvc est un outil de test fourni par Spring Boot qui permettra de 
     * faire des requêtes HTTP vers des routes des contrôleurs (comme on ferait 
     * avec un client HTTP) pour ensuite vérifier si celles ci donnent le résultat attendu
     */
    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        //On lance une requête de type GET vers /api/dog
        mvc.perform(get("/api/dog"))
        //On confirme que la requête a un status de succés
        .andExpect(status().isOk())
        //On utilise la library jsonPath pour vérifier que le json renvoyé correspond
        //à ce à quoi on s'attend, Ici on vérifie que tous les chiens ont une propriété name qui existe
        .andExpect(jsonPath("$[*]['name']").exists())
        //Ici, on fait un test plus précis qui vérifie que la breed du premier chien
        //contient bien la valeur Corgi. (Pas forcément la peine de faire les deux)
        .andExpect(jsonPath("$[0]['breed']").value("Corgi"));
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/dog/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['name']").isString())
        .andExpect(jsonPath("$['breed']").isString())
        .andExpect(jsonPath("$['birthdate']").exists());
    }
    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/dog/1000"))
        .andExpect(status().isNotFound());
    }
    @Test
    void testPostDog() throws Exception {
        mvc.perform(
            post("/api/dog")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name": "New Chien",
                    "breed":"La breed",
                    "birthdate":"2021-02-03"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }
    @Test
    void testPostInvalidDog() throws Exception {
        mvc.perform(
            post("/api/dog")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "breed":"La breed",
                    "birthdate":"2030-02-03"
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteDog() throws Exception {
        mvc.perform(delete("/api/dog/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutDog() throws Exception {
        mvc.perform(put("/api/dog/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "id": 1,
                "name": "Updated Chien",
                "breed":"La breed",
                "birthdate":"2021-02-03"
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['name']").value("Updated Chien"))
        .andExpect(jsonPath("$['breed']").value("La breed"))
        .andExpect(jsonPath("$['birthdate']").value("2021-02-03"));
    }

}
