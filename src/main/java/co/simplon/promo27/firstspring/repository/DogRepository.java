package co.simplon.promo27.firstspring.repository;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import co.simplon.promo27.firstspring.entity.Dog;

@Repository
public class DogRepository {

    @Autowired
    private DataSource dataSource;

    public List<Dog> findAll() {
        List<Dog> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToDog(result));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }


    public Dog findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM dog WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToDog(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }


    private Dog sqlToDog(ResultSet result) throws SQLException {
        return new Dog(
                result.getInt("id"),
                result.getString("name"),
                result.getString("breed"),
                result.getDate("birthdate").toLocalDate());
    }


    public boolean persist(Dog dog) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO dog (name,breed,birthdate) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            if(dog.getBirthdate() != null) {
                stmt.setDate(3, Date.valueOf(dog.getBirthdate()));

            }else {
                stmt.setDate(3, null);
            }

            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                dog.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean update(Dog dog) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE dog SET name=?,breed=?,birthdate=? WHERE id=?");
            stmt.setString(1, dog.getName());
            stmt.setString(2, dog.getBreed());
            if(dog.getBirthdate() != null) {
                stmt.setDate(3, Date.valueOf(dog.getBirthdate()));

            }else {
                stmt.setDate(3, null);
            }
            stmt.setInt(4, dog.getId());

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM dog WHERE id=?");

            
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
}
