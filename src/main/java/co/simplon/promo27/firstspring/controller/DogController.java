package co.simplon.promo27.firstspring.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import co.simplon.promo27.firstspring.entity.Dog;
import co.simplon.promo27.firstspring.repository.DogRepository;
import jakarta.validation.Valid;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/dog") //Permet d'assigner cette route comme prefix de toutes les routes de ce contrôleur
public class DogController {

    @Autowired
    private DogRepository dogRepo;

    @GetMapping
    public List<Dog> all() {
        return dogRepo.findAll();
    }

    @GetMapping("/{id}")
    public Dog one(@PathVariable int id) {
        Dog dog = dogRepo.findById(id);
        if(dog == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return dog;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Dog add( @Valid @RequestBody Dog dog) {
        dogRepo.persist(dog);
        return dog;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); //On relance le one pour renvoyer un 404 si on trouve pas le chien
        dogRepo.delete(id);
    }

    @PutMapping("/{id}")
    public Dog replace(@PathVariable int id, @Valid @RequestBody Dog dog) {
        one(id); //pareil
        dog.setId(id); //On met l'id qui a été donné dans l'url au cas où elle ne correspondraient pas
        dogRepo.update(dog);
        return dog;
    }
}
