package co.simplon.promo27.firstspring.controller;

import java.time.LocalDate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import co.simplon.promo27.firstspring.entity.Dog;
import co.simplon.promo27.firstspring.repository.DogRepository;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;



@RestController
public class FirstController {

    @Autowired
    private DogRepository repo;
    

    @GetMapping("/bonjour")
    public String hello() {
        System.out.println("un message en console dans le contrôleur");
        return "coucou";
    }

    @GetMapping("/bonjour/{name}")
    public String helloToSomeone(@PathVariable String name) {
        System.out.println("le nom entré est "+name);
        return "coucou "+name;
    }

    @GetMapping("/calcul/{a}/{b}")
    public int calcul(@PathVariable int a, @PathVariable int b) {
        return a+b;
    }

    @GetMapping("/puppy")
    public Dog getPuppy() {
        Dog dog = new Dog(1, "Fido", "Corgi", LocalDate.of(2023, 10, 23));
        return dog;
    }

    @PostMapping("/sendpuppy")
    public boolean sendPuppy(@RequestBody Dog dog) {
        //Si le chien envoyé dans le body a bien une breed et que sa valeur est corgi on renvoie true
        if(dog.getBreed() != null && dog.getBreed().equalsIgnoreCase("Corgi")){
            return true;
        }
        return false;
    }
    

    public void utilisationRepoExemple() {

        repo.findAll();
    }

    
    
    
}
