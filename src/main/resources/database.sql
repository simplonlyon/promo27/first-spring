-- Active: 1695817678749@@127.0.0.1@3306@p27_firstspring
DROP TABLE IF EXISTS dog;

CREATE TABLE dog(
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(255) NOT NULL,
    breed VARCHAR(255),
    birthdate DATE
);


INSERT INTO dog (name,breed,birthdate) VALUES 
("Fido", "Corgi", "2021-02-23"),
("Poupoune", "Rotweiller", "2023-01-12"),
("Jean marc", "Poodle", "2014-10-21");