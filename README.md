# First Spring REST API
Première API Rest faite avec Spring Boot en utilisant JDBC pour les repository

**Dépendances du projet :** 
* Spring Web (pour pouvoir faire les contrôleurs)
* Spring Data JDBC (pour pouvoir créer une connexion JDBC via une DataSource `@Autowired` et un application.properties)
* MySQL Driver (Pour pouvoir communiquer avec la base de données mysql/mariadb)
* Validation I/O (Pour pouvoir valider les valeurs des entités, voir plus bas)
* Spring actuator [Optionel] (pour avoir un diagnostique et des informations de débug sur nos routes)
* Spring devtool [Optionel] (pour que le serveur se relance automatiquement à chaque modification de code)

## How To Use
1. Cloner l'application
2. Modifier le [application.properties](src/main/resources/application.properties) et/ou créer la base de données correspondantes
3. Exécuter le [database.sql](src/main/resources/database.sql) sur la base de données en question
4. Lancer le serveur et faire des requêtes sur http://localhost:8080/une-route

## API Rest
Ce backend est donc configuré sous forme d'API Rest, c'est à dire qu'il met à dispositions des ressources sur des routes HTTP afin de pouvoir récupérer des données ou les modifier. Sa vocation est d'être utilisé par une autre application, typiquement une application front end qui se chargera de créer l'interface graphique et qui fera des requêtes HTTP vers ce backend selon les actions qui se passe sur l'API. (Voir [ce fichier](src/main/resources/static/example.html) pour un exemple simple de front end en JS, dont le resultat est accessible sur http://localhost:8080/example.html une fois le serveur lancé)

### Les routes communes d'une API Rest
* **Le GET sur /api/entity** renverra la liste des entités de ce type stockées sur la base de données. En général on fait aussi en sorte d'avoir une pagination et éventuellement une recherche possible sur cette route.
* **Le GET sur /api/entity/1** renverra l'entité correspondant à l'id fourni ou une erreur 404 si aucune entité ne correspond
* **Le POST sur /api/entity** fera persister une nouvelle entité à envoyer en JSON dans le body de la requête et renverra un code 201 (created) ainsi que l'entité qu'on a fait persister complété de son id
* **Le DELETE sur /api/entity/1** supprimera l'entité correspondante (de préférence après vérification que celle ci existe, si non renvoyer un 404) et envoie une réponse vide en 204 (No Content)
* **Le PUT sur /api/entity/1** mettra à jour complétement l'entité correspondant à l'id donné, devra contenir dans son body toutes les valeurs de l'entité ciblée et renverra l'entité mise à jour
* **Le PATCH sur /api/entity/1** mettra à jour partiellement l'entité correspondant à l'id donné, devra contenir les champs à mettre à jour dans l'entité et renverra l'entité complète mise à jour

## Les Tests
Dans cette application, [les tests](src/test/java/co/simplon/promo27/firstspring/controller/) sont des tests fonctionnels ou tests End-To-End (e2e), c'est à dire qu'on test l'application dans sont ensemble proche d'un contexte réel, telle qu'elle serait utilisée par un client.

Ces tests sont les plus proches de la réalité, ce qui est bien, mais c'est également les plus couteux en ressources car ils nécessitent d'avoir une base de données fonctionnelles, une application Spring Boot qui se lance et ils prennent donc généralement plus de temps à être exécutés. Ils sont aussi moins précis.

Dans le contexte d'une API Rest relativement simple comme celle ci ceci dit, ce type de test fait plutôt l'affaire et couvre bien tout le code.

## La Validation
L'entité [Dog](src/main/java/co/simplon/promo27/firstspring/entity/Dog.java) de ce projet a des validation avec la dépendance `Validation I/O`. On rajoute sur chaque propriété qu'on veut valider le validateur nécessaire et côté contrôleur on mettra un `@Valid` sur le `@RequestBody`

### L'entité validée
```java
public class Dog {
    private Integer id;
    @NotBlank
    private String name; //Un name non vide est obligatoire

    private String breed; //Breed sans aucun validateur
    @PastOrPresent
    private LocalDate birthdate; //Si une birthdate est fournie, elle doit être dans le passé ou le présent
    
    ...
```
### Le POST dans le contrôleur
```java
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Dog add( @Valid @RequestBody Dog dog) {
        ...
    }
```
## Exercices
### Première requête post
Dans le FirstController, créer une nouvelle méthode sendPuppy avec un PostMapping sur la route /sendpuppy qui va renvoyer du booléen
Dans les arguments de la méthode, rajouter un argument de type Dog avec une annotation @RequestBody (permet de récupérer les informations du body)
Dans la méthode vérifier si le chien est un Corgi, si oui on renvoie true si non on renvoie false
En utilisant un client HTTP, faire une requête POST sur la route en question et lui mettre dans le body un chien en JSON

### Repo + Contrôleur pour les chiens
#### I. Tous les chiens
Dans le DogRepository, compléter la méthode findAll que j'ai commencée, exactement comme on l'a déjà faite dans d'autres repo
Créer un DogController dans le package co.simplon.promo27.firstspring.controller et dans celui ci, rajouter une propriété private DogRepository avec un @Autowired au dessus (ça permettra de faire automatiquement une instance du repo, donc pas besoin de faire de new)
Dans ce contrôleur, faire un GetMapping sur /api/dog qui va renvoyer une List<Dog> et dedans faire un appel à la méthode findAll du repo
#### II. Un chien spécifique
De retour dans le DogRepository, faire la méthode findById pour récupérer un chien spécifique
Dans le contrôleur, on rajoute un GetMapping sur /api/dog/id avec un PathVariable qui va renvoyer un Dog et on fait appel au findById en utilisant le paramètre de l'url
Faire un if pour vérifier si le chien récupéré par le findById est null, et s'il l'est alors on va throw un 404 avec ce bout de code : throw new ResponseStatusException(HttpStatus.NOT_FOUND);
#### III. Tester la route findById
Dans le DogControllerTest rajouter un nouveau testGetOneSuccess où on fera comme dans le all, mais en lui donnant un id existant et on test pour voir si la réponse a bien la structure voulue
Faire un autre testGetOneNotFound où on fait la même requête mais sur un id inexistant et on lui dit qu'on s'attend à avoir un status notFound
IV. Persist et Post
Dans le repository, rajouter un boolean persist(Dog dog) qui va faire persister un chien, comme dans nos précédent persists, avec juste une attention sur le fait qu'il faudra convertir la LocalDate en Date avec Date.valueOf()
Rajouter une nouvelle route en PostMapping dans le DogController et récupérer le chien dans le body avec le RequestBody comme on avait fait dans le FirstController
Utiliser le repo pour faire persister ce chien